package lzd_client

import (
	"context"
	"crypto/hmac"
	"crypto/sha256"
	"encoding/hex"
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"sort"
	"strings"
	"time"
)

const signMethod = "sha256"

type client struct {
	lzdConfig  LzdConfig
	transport  http.RoundTripper
	httpClient *http.Client
}

func (c *client) DoRequest(
	ctx context.Context,
	platformName string,
	accessToken *string,
	method string,
	apiPath string,
	request interface{},
	result interface{},
) error {
	var (
		req *http.Request
		res *http.Response
		err error
	)

	req, err = c.buildRequest(accessToken, method, platformName, apiPath, request)
	if err != nil {
		return err
	}

	res, err = c.httpClient.Do(req)
	if err != nil {
		return err
	}
	defer res.Body.Close()
	err = c.checkResponseError(res)
	if err != nil {
		return err
	}

	if result == nil {
		return nil
	}

	if res.ContentLength > 0 {
		err := json.NewDecoder(res.Body).Decode(result)

		if err != nil {
			return err
		}

		return nil
	}

	//	in case content-length is not set correctly but we have data in body
	content, err := ioutil.ReadAll(res.Body)
	if err != nil {
		return fmt.Errorf("cannot read response body: %s", err.Error())
	}
	if len(content) > 0 {
		err = json.Unmarshal(content, result)

		if err != nil {
			return err
		}
	}

	return nil
}

func (c *client) buildRequest(
	accessToken *string,
	method string,
	platformName string,
	apiPath string,
	request interface{},
) (req *http.Request, err error) {
	platformConfig := c.lzdConfig.GetConfigOfPlatform()
	baseUrl := c.lzdConfig.GetBaseUrl(platformName)

	timestamp := fmt.Sprintf("%d", time.Now().UnixNano()/(int64(time.Millisecond)/int64(time.Nanosecond)))
	params := map[string]string{
		"timestamp":   timestamp,
		"app_key":     platformConfig.AppKey,
		"sign_method": signMethod,
	}

	if accessToken != nil {
		params["access_token"] = *accessToken
	}

	var newMapRequest map[string]string
	newMapRequest, err = structToMap(request)
	if err != nil {
		return nil, err
	}

	params = mergeMaps(params, newMapRequest)
	sign := c.signRequest(apiPath, params, platformConfig.AppSecret)

	requestUrl := baseUrl + apiPath

	params["sign"] = sign
	requestUrl += "?" + mapToUrlValue(params).Encode()

	req, err = http.NewRequest(method, requestUrl, nil)

	if err != nil {
		return nil, err
	}

	return req, nil
}

func (c *client) checkResponseError(res *http.Response) error {
	if res.StatusCode >= http.StatusBadRequest {
		err := &Error{
			Code: res.StatusCode,
		}
		if res.ContentLength > 0 {
			errDetail, _ := ioutil.ReadAll(res.Body)
			err.Message = string(errDetail)
		}
		return err
	}

	return nil
}

func (c *client) signRequest(apiPath string, params map[string]string, secret string) string {
	keys := make([]string, 0)
	for k := range params {
		keys = append(keys, k)
	}
	sort.Strings(keys)
	for _, k := range keys {
		fmt.Println(k, params[k])
	}
	signature := apiPath
	for _, k := range keys {
		signature += k + params[k]
	}

	hash := hmac.New(sha256.New, []byte(secret))
	hash.Write([]byte(signature))
	return strings.ToUpper(hex.EncodeToString(hash.Sum(nil)))
}

func mergeMaps(maps ...map[string]string) map[string]string {
	result := make(map[string]string)
	for _, m := range maps {
		for k, v := range m {
			result[k] = v
		}
	}
	return result
}

func mapToUrlValue(maps map[string]string) url.Values {
	values := url.Values{}
	for k, v := range maps {
		values[k] = []string{v}
	}

	return values
}

// Converts a struct to a map while maintaining the json alias as keys
func structToMap(obj interface{}) (newMap map[string]string, err error) {
	data, err := json.Marshal(obj) // Convert to a json string

	if err != nil {
		return
	}

	err = json.Unmarshal(data, &newMap) // Convert to a map
	return
}
