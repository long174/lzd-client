package lzd_client

import (
	"github.com/kelseyhightower/envconfig"
)

type LzdConfig struct {
	LZDAuthBaseUrl string `envconfig:"LZD_AUTH_BASE_URL"`
	LZDAppKey      string `envconfig:"LZD_APP_KEY"`
	LZDAppSecret   string `envconfig:"LZD_APP_SECRET"`
}

type PlatformConfig struct {
	AuthBaseUrl string
	AppKey      string
	AppSecret   string
	SGBaseUrl   string
}

func (conf LzdConfig) GetConfigOfPlatform() *PlatformConfig {
	return &PlatformConfig{
		AppKey:    "115631",
		AppSecret: "G0y27IDEusOUJ2U73lOgsn7sRiMdgvOQ",
	}
}

func (conf LzdConfig) GetBaseUrl(platformName string) string {
	platformNames := map[string]string{
		"AUTH":      "https://auth.lazada.com/rest",
		"LAZADA_SG": "https://api.lazada.sg/rest",
	}

	return platformNames[platformName]
}

func GetAppConfigFromEnv() LzdConfig {
	var conf LzdConfig
	envconfig.MustProcess("", &conf)
	return conf
}
