package lzd_client

type OrderItemsResponseDTO struct {
	Data      []OrderItemDetailResponseDTO `json:"data"`
	Code      string                       `json:"code"`
	Message   string                       `json:"message"`
	RequestId string                       `json:"request_id"`
}

type OrderItemDetailResponseDTO struct {
	PaidPrice             float64 `json:"paid_price"`
	ProductMainImage      string  `json:"product_main_image"`
	TaxAmount             float64 `json:"tax_amount"`
	VoucherPlatform       float64 `json:"voucher_platform"`
	Reason                string  `json:"reason"`
	SlaTimeStamp          string  `json:"sla_time_stamp"`
	ProductDetailUrl      string  `json:"product_detail_url"`
	PromisedShippingTime  string  `json:"promised_shipping_time"`
	WarehouseCode         string  `json:"warehouse_code"`
	PurchaseOrderId       string   `json:"purchase_order_id"`
	VoucherSeller         float64  `json:"voucher_seller"`
	ShippingType          string  `json:"shipping_type"`
	CreatedAt             string  `json:"created_at"`
	VoucherCode           string  `json:"voucher_code"`
	PackageId             string  `json:"package_id"`
	Variation             string  `json:"variation"`
	UpdatedAt             string  `json:"updated_at"`
	PurchaseOrderNumber   string  `json:"purchase_order_number"`
	Currency              string  `json:"currency"`
	ShippingProviderType  string  `json:"shipping_provider_type"`
	Sku                   string  `json:"sku"`
	InvoiceNumber         string  `json:"invoice_number"`
	OrderType             string  `json:"order_type"`
	CancelReturnInitiator string  `json:"cancel_return_initiator"`
	ShopSku               string  `json:"shop_sku"`
	IsDigital             int     `json:"is_digital"`
	ItemPrice             float64 `json:"item_price"`
	ShippingServiceCost   float64 `json:"shipping_service_cost"`
	StagePayStatus        string  `json:"stage_pay_status"`
	TrackingCodePre       string  `json:"tracking_code_pre"`
	TrackingCode          string  `json:"tracking_code"`
	ShippingAmount        float64 `json:"shipping_amount"`
	OrderItemId           int   `json:"order_item_id"`
	ReasonDetail          string  `json:"reason_detail"`
	ShopId                string  `json:"shop_id"`
	OrderFlag             string  `json:"order_flag"`
	ReturnStatus          string  `json:"return_status"`
	Name                  string  `json:"name"`
	ShipmentProvider      string  `json:"shipment_provider"`
	VoucherAmount         float64 `json:"voucher_amount"`
	DigitalDeliveryInfo   string  `json:"digital_delivery_info"`
	ExtraAttributes       string  `json:"extra_attributes"`
	OrderId               int   `json:"order_id"`
	Status                string  `json:"status"`
}
