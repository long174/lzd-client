package lzd_client

import (
	"context"
	"net/http"

	"bitbucket.org/snapmartinc/httpclient/middleware"
	log "bitbucket.org/snapmartinc/logger"
)

type Service interface {
	GetCategoryTree(context.Context, string, string) (CategoryResponseDTO, error)
	GetOrders(context.Context, string, string, OrdersRequestDTO) (OrdersResponseDTO, error)
	GetItemsByOrderId(context.Context, string, string, OrderItemsRequestDTO) (OrderItemsResponseDTO, error)
	GetShipmentProviders(context.Context, string, string) (ShipmentProvidersResponseDTO, error)
	GetAccessToken(context.Context, AuthRequestDTO) (resp AuthResponseDTO, err error)
}

type service struct {
	client *client
}

// Ensure client satisfy service interface
var _ Service = &service{}

func NewService() *service {
	c := GetAppConfigFromEnv()
	httpClient := new(http.Client)

	logger := log.NewLoggerFactory(log.InfoLevel).
		Logger(context.TODO())

	transport := http.DefaultTransport
	transport = middleware.WithMiddleware(
		transport,
		middleware.NewRequestLogger(logger),
		middleware.NewResponseLogger(logger),
	)

	httpClient.Transport = transport

	client := &client{
		lzdConfig:  c,
		httpClient: httpClient,
	}

	return &service{
		client: client,
	}
}

// Get category tree
func (s *service) GetCategoryTree(
	ctx context.Context,
	platformName string,
	accessToken string,
) (resp CategoryResponseDTO, err error) {
	err = s.client.DoRequest(ctx, platformName, &accessToken, "GET", "/category/tree/get", nil, &resp)

	return resp, err
}

// Get orders tree
func (s *service) GetOrders(
	ctx context.Context,
	platformName string,
	accessToken string,
	request OrdersRequestDTO,
) (resp OrdersResponseDTO, err error) {
	err = s.client.DoRequest(ctx, platformName, &accessToken, "GET", "/orders/get", request, &resp)

	return resp, err
}

// Get orders tree
func (s *service) GetItemsByOrderId(
	ctx context.Context,
	platformName string,
	accessToken string,
	request OrderItemsRequestDTO,
) (resp OrderItemsResponseDTO, err error) {
	err = s.client.DoRequest(ctx, platformName, &accessToken, "GET", "/order/items/get", request, &resp)

	return resp, err
}

// Get orders tree
func (s *service) GetShipmentProviders(
	ctx context.Context,
	platformName string,
	accessToken string,
) (resp ShipmentProvidersResponseDTO, err error) {
	err = s.client.DoRequest(ctx, platformName, &accessToken, "GET", "/shipment/providers/get", nil, &resp)

	return resp, err
}

// Get access token
func (s *service) GetAccessToken(
	ctx context.Context,
	request AuthRequestDTO,
) (resp AuthResponseDTO, err error) {
	err = s.client.DoRequest(ctx, "AUTH", nil, "GET", "/auth/token/create", request, &resp)

	return resp, err
}
