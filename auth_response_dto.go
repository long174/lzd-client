package lzd_client

type AuthResponseDTO struct {
	AccessToken  string `json:"access_token"`
	RefreshToken string `json:"refresh_token"`
	Code         string `json:"code"`
	Message      string `json:"message"`
	RequestId    string `json:"request_id"`
}
