package lzd_client

type OrdersResponseDTO struct {
	Data      OrderResponseDTO `json:"data"`
	Code      string           `json:"code"`
	Message   string           `json:"message"`
	RequestId string           `json:"request_id"`
}

type OrderResponseDTO struct {
	Order []OrderDetailResponseDTO `json:"orders"`
	Count int                      `json:"count"`
}

type OrderDetailResponseDTO struct {
	VoucherPlatform            float64                      `json:"voucher_platform"`
	Voucher                    float64                      `json:"voucher"`
	WarehouseCode              string                       `json:"warehouse_code"`
	OrderNumber                int                          `json:"order_number"`
	VoucherSeller              float64                      `json:"voucher_seller"`
	CreatedAt                  string                       `json:"created_at"`
	VoucherCode                string                       `json:"voucher_code"`
	GiftOption                 bool                         `json:"gift_option"`
	CustomerLastName           string                       `json:"customer_last_name"`
	UpdatedAt                  string                       `json:"updated_at"`
	PromisedShippingTimes      string                       `json:"promised_shipping_times"`
	Price                      string                       `json:"price"`
	NationalRegistrationNumber string                       `json:"national_registration_number"`
	PaymentMethod              string                       `json:"payment_method"`
	AddressUpdatedAt           string                       `json:"address_updated_at"`
	CustomerFirstName          string                       `json:"customer_first_name"`
	ShippingFee                float64                      `json:"shipping_fee"`
	BranchNumber               string                       `json:"branch_number"`
	TaxCode                    string                       `json:"tax_code"`
	ItemsCount                 int                          `json:"items_count"`
	DeliveryInfo               string                       `json:"delivery_info"`
	Statuses                   []string                     `json:"statuses"`
	ExtraAttributes            string                       `json:"extra_attributes"`
	OrderId                    int                          `json:"order_id"`
	GiftMessage                string                       `json:"gift_message"`
	Remarks                    string                       `json:"remarks"`
	AddressShipping            AddressShipping              `json:"address_shipping"`
	Items                      []OrderItemDetailResponseDTO `json:"items"`
}

type AddressShipping struct {
	Country   string `json:"country"`
	Address3  string `json:"address3"`
	Address2  string `json:"address2"`
	City      string `json:"city"`
	Phone     string `json:"phone"`
	Address1  string `json:"address1"`
	PostCode  string `json:"post_code"`
	Phone2    string `json:"phone2"`
	LastName  string `json:"last_name"`
	Address5  string `json:"address5"`
	Address4  string `json:"address4"`
	FirstName string `json:"first_name"`
}
