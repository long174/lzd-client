package lzd_client_test

import (
	"bitbucket.org/long174/lzd-client"
	"context"
	"fmt"
	"reflect"
	"testing"
)

const token = "50000000a33NGS7iBuVPSeb5lYihyhySFZhfQ5qyyDnfv14cdcbc5gWjxovT03m"

func TestGetCategoryTree(t *testing.T) {

	c := lzd_client.NewService()
	result, err := c.GetCategoryTree(context.Background(), "LAZADA_SG", token)

	if err != nil {
		t.Error("expected nil but got: ", err)
	}

	expected := lzd_client.CategoryResponseDTO{
		Data: []lzd_client.CategoryNodeDTO{
			{
				Children: []lzd_client.CategoryNodeDTO{{
					Children: []lzd_client.CategoryNodeDTO{{
						Var:        false,
						Name:       "Accessories",
						Leaf:       true,
						CategoryId: 10001958,
					}},
					Var:        false,
					Name:       "Kids Bags",
					Leaf:       false,
					CategoryId: 10001930,
				}},
				Var:        false,
				Name:       "Watches Sunglasses Jewellery",
				Leaf:       false,
				CategoryId: 7686,
			},
		},
		Code:      "0",
		RequestId: "0b8ae2f415335521390863717",
	}
	if !reflect.DeepEqual(expected, result) {
		t.Error("expected ", expected, " but got: ", result)
	}
}

func TestGetOrders(t *testing.T) {

	c := lzd_client.NewService()
	orderRequest := lzd_client.OrdersRequestDTO{
		CreatedAfter: "2018-02-10T16:00:00+08:00",
	}
	result, err := c.GetOrders(context.Background(), "LAZADA_SG", token, orderRequest)

	if err != nil {
		t.Error("expected nil but got: ", err)
	}

	fmt.Println(result)
}

func TestGetOrderItems(t *testing.T) {

	c := lzd_client.NewService()
	orderItemRequest := lzd_client.OrderItemsRequestDTO{
		OrderId: "25423511230732",
	}
	result, err := c.GetItemsByOrderId(context.Background(), "LAZADA_SG", token, orderItemRequest)

	if err != nil {
		t.Error("expected nil but got: ", err)
	}

	fmt.Println(result)
}

func TestGetShipmentProviders(t *testing.T) {

	c := lzd_client.NewService()
	result, err := c.GetShipmentProviders(context.Background(), "LAZADA_SG", token)

	if err != nil {
		t.Error("expected nil but got: ", err)
	}

	fmt.Println(result)
}

func TestGetAccessToken(t *testing.T) {
	authRequest := lzd_client.AuthRequestDTO{
		Code: "0_115631_pvF7ifPKmOmXnwaWs2O2IeSf18292",
	}

	c := lzd_client.NewService()
	result, err := c.GetAccessToken(context.Background(), authRequest)

	if err != nil {
		t.Error("expected nil but got: ", err)
	}

	fmt.Println(result)
}
