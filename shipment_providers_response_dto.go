package lzd_client

type ShipmentProvidersResponseDTO struct {
	Data      ShipmentProviderResponseDTO `json:"data"`
	Code      string                      `json:"code"`
	Message   string                      `json:"message"`
	RequestId string                      `json:"request_id"`
}

type ShipmentProviderResponseDTO struct {
	ShipmentProviders []ShipmentProviderDetailResponseDTO `json:"shipment_providers"`
}

type ShipmentProviderDetailResponseDTO struct {
	Name                        string `json:"name"`
	IsDefault                   int    `json:"is_default"`
	TrackingCodeExample         string `json:"tracking_code_example"`
	EnabledDeliveryOptions      string `json:"enabled_delivery_options"`
	Cod                         int    `json:"cod"`
	TrackingCodeValidationRegex string `json:"tracking_code_validation_regex"`
	TrackingUrl                 string `json:"tracking_url"`
	ApiIntegration              int    `json:"api_integration"`
}
